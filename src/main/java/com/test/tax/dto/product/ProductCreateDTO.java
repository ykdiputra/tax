package com.test.tax.dto.product;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCreateDTO {
	
	private String prdName;
	private BigDecimal prdPrc;
	private int taxParam;
	
}
