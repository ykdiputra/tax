FROM openjdk:8
MAINTAINER ykdiputra

ADD target/taxcalc-app.jar taxcalc-app.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "taxcalc-app.jar"]


