# Test Case

Tax Calculator

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
```
Local Run:

1. Java 1.8
2. PostgreSQL
3. Maven
4. Apache Tomcat
```
```
Docker Run:

1. Docker 
2. Docker Image:
	- Download PostgreSQL download from docker hub: docker pull postgres
```

### Installing
```
- Download and extract to your selected folder
- Open your favourite java editor (here I use Spring Tool Suite)
- Choose Import > Existing Maven Project and browse to extracted directory
- Execute Maven > Update Project
```
## Running the tests
```
Using JUnit Test, ProductTest.java
```

## API Documentation
```
http://localhost:8085/tax/swagger-ui.html
```
## Deployment
```
- Create taxcalc-app.jar file
- Open command propmt and build docker image: docker build -f Dockerfile -t taxcalc-app .
- Execute: docker images 
  to make sure your image is done
- After image is created and exist in container, execute command: docker-compose up -d
- After finish, access http://<virtual_host_ip>/tax/swagger-ui.html
```